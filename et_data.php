<?php
		include('simple_html_dom.php');
		header("Content-Type: application/json");
		$timestamp = time();
		$debug = true;

		if(date('D', $timestamp) === 'Mon' || $_GET['overwrite'] == "1" || $debug )
		{
			$data = file_get_html('http://www.roguard.net/game/endless-tower/');
			$data_mini = file_get_html('https://www.roguard.net/game/endless-tower/#MINI');
			
			$etAry = getJSONData($data_mini);

			echo json_encode($etAry);

			//Saving JSON File
			$file = fopen('et_data.json','w') or die('Error');
			fwrite($file,json_encode($etAry));
			fclose($file);
		}else
		{
			echo file_get_contents('et_data.json');
		}

		function getJSONData($data)
		{
			$tableDat = $data->find('div[id="et-mvp-list"] div[class="table-responsive"]',0);
			
			$tableDat_mini = $data->find('div[id="et-mvp-list"] div[class="table-responsive"]',0);

			$etAry = array();
			$flrAry = array("10","20","30","40","50","60","70","80","90","100");
			$flrAry_mini = array("3","6","13","16","23","26","33","36","43","46","53","56","63","66","73","76","83","86","93","96");

			foreach($tableDat->find('tr') as $tr )
			{
				$et_id = null;
				$floor_data = array();
				$en_data = array();
				$i = -1;
				foreach($tr->find('td') as $td)
				{
					if(sizeof($td->find('a')) == 0 && !sizeof($td->find('img')) > 0)
						$et_id = $td->innertext;
					else
					{
						if(sizeof($td->find('a')) == 0)
							array_push($floor_data,array("floor"=>$flrAry[$i],"mobinfo" => ("No Data Available -  " . sizeof($td->find('img')) . " Bosses.")));
						else
						{
							$boss = "";
							$boss_img = array();
							foreach($td->find('a') as $bossDat){
								$data_content = $bossDat->attr["data-content"];
								preg_match('/(<strong>).+(<\/strong>)/', $data_content, $matches);
								if($matches)
									$boss .= str_replace("</strong>","",str_replace("<strong>","",$matches[0])).", ";
								foreach($bossDat->find('img') as $img)
									array_push($boss_img,$img->attr["src"]);
							}

							array_push($floor_data,array("floor"=> $flrAry[$i],"mobinfo" => substr($boss,0,-2),"mob_img" => $boss_img));
						}
					}
					$i++;
				}
				if($et_id != null)
				{
					$en_data["EN_DAT"] = $et_id;
					$en_data["floors"] = $floor_data;
					array_push($etAry,$en_data);
				}
			}
			return $etAry;
		}
?>